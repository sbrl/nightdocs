"use strict";

import path from 'path';

/**
 * Generates various VT100 ANSI escape sequences.
 * Ported from C#.
 * Link to original: https://gist.github.com/a4edd3204a03f4eedb79785751efb0f3#file-ansi-cs
 */
class Ansi {
	constructor() {
		/**
		 * Whether we should *actually* emit ANSI escape codes or not.
		 * Useful when we want to output to a log file, for example
		 * @type {Boolean}
		 */
		this.enabled = true;
		
		this.escape_codes();
	}
	
	escape_codes() {
		// Solution on how to output ANSI escape codes in C# from here:
		// https://www.jerriepelser.com/blog/using-ansi-color-codes-in-net-console-apps
		this.reset = this.enabled ? "\u001b[0m" : "";
		this.hicol = this.enabled ? "\u001b[1m" : "";
		this.locol = this.enabled ? "\u001b[2m" : "";
		this.underline = this.enabled ? "\u001b[4m" : "";
		this.inverse = this.enabled ? "\u001b[7m" : "";
		this.fblack = this.enabled ? "\u001b[30m" : "";
		this.fred = this.enabled ? "\u001b[31m" : "";
		this.fgreen = this.enabled ? "\u001b[32m" : "";
		this.fyellow = this.enabled ? "\u001b[33m" : "";
		this.fblue = this.enabled ? "\u001b[34m" : "";
		this.fmagenta = this.enabled ? "\u001b[35m" : "";
		this.fcyan = this.enabled ? "\u001b[36m" : "";
		this.fwhite = this.enabled ? "\u001b[37m" : "";
		this.bblack = this.enabled ? "\u001b[40m" : "";
		this.bred = this.enabled ? "\u001b[41m" : "";
		this.bgreen = this.enabled ? "\u001b[42m" : "";
		this.byellow = this.enabled ? "\u001b[43m" : "";
		this.bblue = this.enabled ? "\u001b[44m" : "";
		this.bmagenta = this.enabled ? "\u001b[45m" : "";
		this.bcyan = this.enabled ? "\u001b[46m" : "";
		this.bwhite = this.enabled ? "\u001b[47m" : "";
		
		this.url_start = '\u001b]8;;';
		this.url_display_text = '\u001b\\';
		this.url_end = '\u001b]8;;\u001b\\';
	}
	
	// Thanks to http://ascii-table.com/ansi-escape-sequences.php for the following ANSI escape sequences
	up(lines = 1) {
		return this.enabled ? `\u001b[${lines}A` : "";
	}
	down(lines = 1) {
		return this.enabled ? `\u001b[${lines}B` : "";
	}
	right(lines = 1) {
		return this.enabled ? `\u001b[${lines}C` : "";
	}
	left(lines = 1) {
		return this.enabled ? `\u001b[${lines}D` : "";
	}
	
	jump_to(x, y) {
		return this.enabled ? `\u001b[${y};${x}H` : "";
	}
	
	cursor_pos_save() {
		return this.enabled ? `\u001b[s` : "";
	}
	cursor_pos_restore() {
		return this.enabled ? `\u001b[u` : "";
	}
	
	url(href, display_text) {
		return this.enabled ? `${this.url_start}${href}${this.url_display_text}${display_text}${this.url_end}` : "";
	}
	
	url_file(filepath) {
		if(!this.enabled) return "";
		let filepath_abs = path.resolve(process.cwd(), filepath);
		return this.url(`file://${filepath_abs}`, filepath);
	}
}

export default Ansi;
