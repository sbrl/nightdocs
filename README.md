# nightdocs

> A lightweight blazingly-fast markdown-powered static site generator for your next project.

A simple static site generator for your next project's documentation. Converts a folder of [Markdown](https://github.github.com/gfm/) to static HTML that you can host on a web server.

## Getting Started
Install with npm:

```bash
npm install --save-dev nightdocs
```

-----

Organise your documentation like this:

 - `docs/`
	 - `README.md` - Contents of the left-hand sidebar
	 - `some.md`
	 - `more.md`
	 - `documentation.md`
	 - `files.md`

-----

Then, write a toml config file like this:

```toml
[template]
project_name = "Sheep Counter Plus"
logo_src = "https://bobsrockets.com/favicon.png"

[directories]
# Path to docs (input) folder
docs = "./docs" # Default value
# Path to build (output) folder
output = "./__nightdocs" # Default value

[theme]
# Define only 1 of the following:

# Use a built-in theme. Possible values: nightdocs (default), basic
builtin = "nightdocs"
# Use a specific file instead
file = "path/to/theme.css"

```

-----

Then execute:

```bash
node_modules/.bin/nightdocs --config path/to/config.toml
```

## Contributing
Contributions are welcome! Simply fork this repository, make your changes, and submit a Pull Request (aka Merge Request).

All contributions must be declared to have the `Mozilla Public License 2.0` (the same license that this repository is under).

## License
The lantern build engine is licensed under the _Mozilla Public License 2.0_ (MPL-2.0). This license can be found in the _LICENSE_ file in this repository, along with a link to an easy-to-read summary.
