"use strict";

import fs from 'fs';
import path from 'path';

import { marked } from 'marked';
import nightink from 'nightink';

import CLIHelper from './Helpers/CLIHelper.mjs';
import object_apply_deep from './Helpers/ObjectApplicator.mjs';

class NightDocs {
	constructor(in_settings) {
		this.settings = in_settings;
		this.ansi = this.settings.ansi;
		this.package_json = JSON.parse(fs.readFileSync(
			path.resolve(this.settings.directories.ours, "./package.json")
		));
		
		this.cli = new CLIHelper();
	}
	
	async read_template() {
		this.template = await fs.promises.readFile(
			path.resolve(this.settings.directories.ours, "./templates/index.html"),
			"utf-8"
		);
	}
	
	async run() {
		if(!fs.existsSync(this.settings.directories.docs)) {
			console.error(`Error: The couldn't find the target docs to render at '${this.settings.directories.docs}' - no such file or directory`);
			process.exit(1);
		}
		let files = await fs.promises.readdir(this.settings.directories.docs);
		let files_docs = files.filter((filename) =>
			filename.endsWith(".md") &&
			path.basename(filename).toLowerCase() !== "readme.md"
		);
		let file_index = files.find((filename) => path.basename(filename).toLowerCase() == "readme.md");
		
		// this.cli.log_time(`Using index file ${this.ansi.hicol}${this.ansi.url_file(file_index)}${this.ansi.reset}.`);
		// Make sure the directory exists first
		await fs.promises.mkdir(this.settings.directories.output, { recursive: true });
		await Promise.all([
			this.read_template(),
			this.render_navigation(file_index),
			this.copy_theme()
		]);
		
		await this.render_pages(files_docs);
		
		this.cli.log_time(`${this.ansi.hicol}${this.ansi.fgreen}Rendered docs to ${this.ansi.hicol}${this.ansi.url_file(this.settings.directories.output)}${this.ansi.reset}`);
	}
	
	async copy_theme() {
		let target_filename = path.resolve(this.settings.directories.output, `./${this.settings.theme.builtin}.css`);
		let source_filename = path.resolve(this.settings.directories.ours, "./templates/nightdocs.css");
		
		if(typeof this.settings.theme.file !== "undefined")
			source_filename = this.settings.theme.file;
		
		if(!fs.existsSync(source_filename))
			throw new Error(`[copying theme] Error: Source file ${source_filename} was not found.`);
		
		await fs.promises.copyFile(source_filename, target_filename);
	}
	
	
	async render_navigation(index_filename) {
		this.navigation = marked(await fs.promises.readFile(
			path.resolve(this.settings.directories.docs, index_filename),
			"utf-8"
		)).replace(/\.md/g, ".html");
	}
	
	async render_pages(files_docs) {
		let work = [];
		let i = 1;
		for(let doc_file of files_docs) {
			this.cli.log_time(`[ ${i} / ${files_docs.length} ] Processing ${doc_file}`);
			work.push(fs.promises.writeFile(
				`${this.settings.directories.output}/${path.basename(doc_file).replace(".md", ".html")}`,
				await this.render_page(doc_file)
			));
			
			i++;
		}
		
		await Promise.all(work);
	}
	
	async render_page(page_filename) {
		let options = {
			navigation: this.navigation,
			content: marked(await fs.promises.readFile(
				path.resolve(this.settings.directories.docs, page_filename),
				"utf-8"
			)),
			build_date: new Date().toDateString(),
			package: this.package_json
		};
		object_apply_deep(this.settings.template, options);
		return nightink(this.template, options);
	}
}

export default NightDocs;
