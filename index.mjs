#!/usr/bin/env node

import path from 'path';
import fs from 'fs';

import Toml from 'toml';

import Ansi from './Ansi.mjs';
import NightDocs from './NightDocs.mjs';
import CLIHelper from './Helpers/CLIHelper.mjs';
import object_apply_deep from './Helpers/ObjectApplicator.mjs';

// 1: Setup
const ansi = new Ansi();
const cli_helper = new CLIHelper(ansi);

const settings = {
	program_name: "NightDocs",
	version: `v1.0.7`, // TODO: Read this from package.json
	description: "generates static html from your markdown documentation",
	ansi,
	
	error_lines_context: 5,
	
	// ---------
	
	theme: {
		builtin: "nightdocs"
	},
	directories: {
		ours: path.dirname(fs.realpathSync(process.argv[1])),
		docs: "./docs",
		output: "./__nightdocs"
	},
	template: {
		project_name: "NightDocs",
		logo_src: "https://gitlab.com/sbrl/nightdocs/raw/master/logo.png"
	}
};

// 2: CLI Argument Parsing
let args = process.argv.slice(2); // Slice out the node binary name and the filename
let extras = [];
for(let i = 0; i < args.length; i++) {
	if(!args[i].startsWith("-")) {
		extras.push(args[i]);
		continue;
	}
	
	switch(args[i]) {
		case "--help":
		case "-h":
			console.log(`${settings.program_name}, ${settings.version}
	${ansi.locol}By Starbeamrainbowlabs${ansi.reset}

${ansi.hicol}This program ${settings.description}.${ansi.reset}

${ansi.fblue}${ansi.hicol}Usage:${ansi.reset}
	node ${process.argv[1]}

${ansi.fblue}${ansi.hicol}Options:${ansi.reset}
	${ansi.fyellow}-h	--help		${ansi.reset}Show this message
	${ansi.fyellow}-v	--version	${ansi.reset}Show the version of this program
	${ansi.fyellow}-c	--config	${ansi.reset}Use the specified TOML config file

${ansi.fblue}${ansi.hicol}Environment Variables:${ansi.reset}
	(none yet)
`);
			process.exit();
			break;
		
		case "--version":
		case "-v":
			console.log(program.version);
			break;
		
		case "-c":
		case "--config":
			let config_filename = args[++i];
			if(!fs.existsSync(config_filename)) {
				cli_helper.error(`Error 1: Couldn't find config file at '${config_filename}' because it doesn't exist.`);
				process.exit(1);
			}
			let config = fs.readFileSync(config_filename, "utf-8");
			try {
				config = Toml.parse(config);
			} catch(error) {
				cli_helper.error(`Error 2: Toml parsing error: ${error.message}`);
				console.error(`*** ${ansi.hicol}${ansi.url_file(config_filename)}:${error.line}:${error.column}${ansi.reset} ***`);
				
				let config_lines = config.split("\n");
				for(let i = Math.max(1, error.line - settings.error_lines_context); i < Math.min(config_lines.length, error.line + settings.error_lines_context); i++) {
					console.error(`${ansi.locol}${i.toString().padStart(4)}.${ansi.reset} ${config_lines[i-1]}`);
					if(i == error.line)
						console.error(`${ansi.fblue}${ansi.hicol}${"-".repeat(4+2+error.column-1)}^${ansi.reset}`);
				}
				process.exit(2);
			}
			
			// Apply the settings we've just loaded
			object_apply_deep(config, settings);
			
			break;
		
		// Add more arguments here
	}
}

// 3: Environment Variable Parsing

// process.env.XYZ

// 4: Run
const nightdocs = new NightDocs(settings);
nightdocs.run();

// 5: Cleanup
