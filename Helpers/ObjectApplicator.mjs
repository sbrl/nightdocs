"use strict";

function object_apply_deep(source, target) {
	for(let key in source) {
		// If the target already has an object for this key, recurse
		if(typeof source[key] == "object" && typeof target[key] == "object")
		 	object_apply_deep(source[key], target[key]);
		else // If not, then we don't need to do anything special
			target[key] = source[key];
	}
}

export default object_apply_deep;
