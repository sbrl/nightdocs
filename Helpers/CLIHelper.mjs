"use strict";

class CLIHelper {
	constructor(in_ansi) {
		this.ansi = in_ansi;
		this.start_time = new Date();
	}
	
	error(message) {
		console.error(`${this.ansi.fred}${this.ansi.hicol}${message}${this.ansi.reset}`);
	}
	
	log_time(...args) {
		console.log(`[${this.time_since_start().toFixed(3)}] ${args[0]}`, ...args.slice(1));
	}
	
	time_since_start() {
		return (new Date() - this.start_time) / 1000;
	}
}

export default CLIHelper;
